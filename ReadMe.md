# Spring Boot


## Einführung von Spring Boot für Entwickler:

### I. Einführung in Spring Boot

#### A. Was ist Spring Boot?

**Spring Boot** ist ein Framework für die Java-Entwicklung, das auf dem **Spring Framework** aufbaut und die
Entwicklung von Enterprise-Anwendungen vereinfacht. Es bietet eine einfachere und schnellere Möglichkeit,
Java-Anwendungen zu erstellen und zu konfigurieren, indem es eine Reihe von **vorkonfigurierten Komponenten**
bereitstellt,
die häufig in Java-Webanwendungen verwendet werden. Spring Boot kann für die Entwicklung von Webanwendungen,
Microservices und anderen Arten von Java-Anwendungen verwendet werden.

Eine der Hauptfunktionen von Spring Boot ist die **automatische Konfiguration**,
die die Konfiguration von Spring-Anwendungen vereinfacht und beschleunigt.
Darüber hinaus vereinfacht Spring Boot auch den Umgang mit Abhängigkeiten und ermöglicht es Entwicklern,
schnell auf neue Anforderungen zu reagieren.

Insgesamt ist Spring Boot ein Framework, das die Entwicklung von Java-Anwendungen erleichtert und beschleunigt,
indem es eine Reihe von vorkonfigurierten Komponenten bereitstellt,
die häufig in Enterprise-Anwendungen verwendet werden.


#### B. Warum Spring Boot?

> 1. Schnellere Entwicklung: Spring Boot vereinfacht die Konfiguration von Java-Anwendungen durch automatische
     Konfiguration und bietet eine integrierte Entwicklungsumgebung, die das Erstellen und Bereitstellen von
     Anwendungen erleichtert. Dadurch können Entwickler schneller Anwendungen entwickeln.

> 2. Weniger Boilerplate-Code: Spring Boot reduziert die Menge an Boilerplate-Code, der geschrieben werden muss,
     indem es eine Reihe von vorkonfigurierten Komponenten bereitstellt, die häufig in Java-Webanwendungen
     verwendet werden. Dadurch können Entwickler sich auf die eigentliche Geschäftslogik der Anwendung konzentrieren.

> 3. Bessere Skalierbarkeit: Spring Boot ist für die Entwicklung von Microservices optimiert und bietet Tools und
     Komponenten, die die Skalierbarkeit von Anwendungen verbessern.

> 4. Integration mit anderen Spring-Tools: Spring Boot ist Teil des Spring-Ökosystems und lässt sich nahtlos mit
     anderen Spring-Tools wie Spring Data und Spring Cloud integrieren.

> 5. Community-Unterstützung: Spring Boot hat eine große und aktive Community, die regelmäßig Updates und
     Erweiterungen veröffentlicht und Probleme schnell löst.

Insgesamt ist Spring Boot eine leistungsstarke und beliebte Wahl für Entwickler, die Java-Anwendungen entwickeln möchten, da es die Entwicklung erleichtert, die Skalierbarkeit verbessert und eine aktive Community-Unterstützung bietet.

#### C. Vorteile von Spring Boot gegenüber Java EE

> 1. Einfachere Konfiguration: Spring Boot bietet automatische Konfiguration, die die Konfiguration von
     Java-Anwendungen vereinfacht. Im Gegensatz dazu erfordert Java EE eine manuelle Konfiguration, die zeitaufwändig sein kann.

> 2. Weniger Boilerplate-Code: Spring Boot reduziert die Menge an Boilerplate-Code, der geschrieben werden muss,
     indem es vorkonfigurierte Komponenten bereitstellt. Java EE erfordert mehr Boilerplate-Code, was die Entwicklung verlangsamen kann.

> 3. Bessere Unterstützung für Microservices: Spring Boot ist speziell für die Entwicklung von Microservices
     konzipiert und bietet Tools und Komponenten, die die Skalierbarkeit von Anwendungen verbessern.
     Java EE ist weniger auf Microservices ausgerichtet.

> 5. Einfachere Integration mit anderen Spring-Tools: Spring Boot ist Teil des Spring-Ökosystems und lässt sich
     nahtlos mit anderen Spring-Tools wie Spring Data und Spring Cloud integrieren. Java EE hat eine geringere
     Integration mit anderen Frameworks.

> 5. Aktivere Community-Unterstützung: Spring Boot hat eine große und aktive Community, die regelmäßig Updates und
     Erweiterungen veröffentlicht und Probleme schnell löst. Java EE hat in den letzten Jahren eine geringere Unterstützung erfahren.
     Insgesamt bietet Spring Boot viele Vorteile gegenüber Java EE, darunter eine einfachere Konfiguration, weniger Boilerplate-Code, bessere Unterstützung für Microservices, einfachere Integration mit anderen Spring-Tools und eine aktive Community-Unterstützung.


### II. Konfiguration von Spring Boot

#### A. Initialisierung eines Spring Boot Projektes

Die Initialisierung von Spring Boot ist einfach und unkompliziert.
Hier sind die Schritte, um Spring Boot zu initialisieren:

> 1. Installiere zuerst die neueste Version von Java Development Kit (JDK) auf Ihrem Computer, wenn Sie dies noch
     nicht getan haben. Spring Boot 2.x erfordert mindestens Java 8, **Spring Boot 3** erfordert mindestens **Java 17**.

> 2. Öffnen der offiziellen Spring Boot-Website unter https://spring.io/projects/spring-boot.

> 3. Spring Boot Applikationen können über die **Spring Starter** Seite https://start.spring.io
     als Template erstellt werden.
     Hier werden die Pakete, die innerhalb der Applikation verwendet werden sollen initial gewählt. Auch das
     Projekt/Paket Management (Maven/Gradle) kann hier vordefiniert werden. Im Anschluss kann ein ZIP-File
     heruntergeladen werden, dass das Starter-Projekt für die Applikation enthält.

![Spring Boot Starter](starter.png)

Das sind die grundlegenden Schritte, um Spring Boot zu initialisieren. Es ist auch möglich, Spring Boot als Abhängigkeit
in einem bestehenden Projekt zu verwenden, indem es einfach zu der build.gradle oder pom.xml hinzugefügt wird.
Weitere Informationen zur Verwendung von Spring Boot können in der offiziellen Spring-Dokumentation gefunden werden. https://spring.io/projects/spring-boot


#### B. Konfiguration von Spring Boot

Spring Boot bietet eine automatische Konfiguration, die die Konfiguration von Anwendungen vereinfacht.
Die Konfiguration von Spring Boot kann jedoch auch manuell anpassen, um spezifische Anforderungen der Anwendung zu
erfüllen. Hier sind einige Schritte, um Spring Boot manuell zu konfigurieren:

> Erstellen einer Konfigurationsklasse:
>> Erstellen einer Konfigurationsklasse, indem die @Configuration-Annotation verwendet wird. Es kann auch
@Bean-Annotationen verwendet werden, um benutzerdefinierte Komponenten zu definieren, die von Spring Boot verwaltet
werden sollen.

#### SinkConfig.java Beispiel - Alle 5 Sekunden wird ein TimeDto Objekt mit der aktuellen Uhrzeit emittet
```java
import cool.cfapps.firebasedemo.dto.TimeDto;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;

import java.util.Date;

@Configuration
public class SinkConfig {

    @Bean
    public Sinks.Many<TimeDto> sink(){
        return Sinks.many().replay().limit(1);
    }

    @Bean
    public Flux<TimeDto> dateTimeBroadCast(Sinks.Many<TimeDto> sink){
        return sink.asFlux();
    }

    // Die @Scheduled Annotation erzeugt einen Scheduler, hier alle 5 Sekunden
    @Scheduled(cron="*/5 * * * * ?")
    public void demoServiceMethod()
    {
        sink().tryEmitNext(
                new TimeDto(new Date().toString())
        );
    }
}
```

>Konfigurieren der Anwendungseigenschaften:
>>Anwendungseigenschaften könnwn in einer application.properties- oder application.yml-Datei definiert werden.
Es können die Standardanwendungseigenschaften von Spring Boot überschrieben und neue Eigenschaften hinzufgefügt
werden, um weitere Anforderungen zu erfüllen.

#### application.properties Beispiel
``` properties
spring.datasource.url=jdbc:mariadb://localhost:3306/ordering
spring.datasource.username=user
spring.datasource.password=password
spring.datasource.driver-class-name=org.mariadb.jdbc.Driver

# Hibernate ddl auto (create, create-drop, update): with "update" the database
# schema will be automatically updated accordingly to java entities found in
# the project
spring.jpa.hibernate.ddl-auto=update

# Allows Hibernate to generate SQL optimized for a particular DBMS
spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.MariaDBDialect
```

>Verwendung von Profil-basierter Konfiguration:
>>Spring Boot bietet Profil-basierte Konfiguration, die es ermöglicht,
unterschiedliche Konfigurationen für verschiedene Umgebungen zu definieren.
Es können beispielsweise eine Konfiguration für die Entwicklung und eine andere
Konfiguration für die Produktion erstellt werden.
Profil abhängige Konfigurationen können an verschiedenen Stellen im Code und in der Konfiguration
verwendet werden. Mehr hierzu unter: https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#features.profiles


application-dev.properties file -> Profileigenschaften für die Entwicklung
``` properties
spring.datasource.driver-class-name=org.h2.Driver
spring.datasource.url=jdbc:h2:mem:db;DB_CLOSE_DELAY=-1
spring.datasource.username=sa
spring.datasource.password=sa
```

application-production.properties -> Profileigenschaften für die Produktion
``` properties
spring.datasource.driver-class-name=com.mysql.cj.jdbc.Driver
spring.datasource.url=jdbc:mysql://localhost:3306/db
spring.datasource.username=user
spring.datasource.password=password
```

Zum setzen des aktiven Profiles ist eine Methode das Setzen in den Standart-Properties
``` properties
spring.profiles.active=dev
```

Eine weitere gute Dokumentation kann hier gefunden werden: https://www.baeldung.com/spring-profiles

> Konfiguration des Webservers:
>> Spring Boot verwendet Tomcat als Standard-Webserver.
Es kann jedoch auch ein anderer Webserver wie Jetty verwendet werden.
Ebenfalls können auch die Portnummern und viele weitere Parameter konfiguriert werden, auf der die Anwendung läuft.

Dazu muss die **pom.xml** aktualisiert und die Abhängigkeit für **spring-boot-starter-jetty** hinzufügt werden.
Außerdem muss die standardmäßig hinzugefügte **spring-boot-starter-tomcat**-Abhängigkeit ausgeschlossen werden.

``` xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
    <exclusions>
        <exclusion>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-tomcat</artifactId>
        </exclusion>
    </exclusions>
</dependency>
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-jetty</artifactId>
</dependency>
```
Oder bei Verwendunung von gradle muss die **gradle.properties** angepasst werden.

``` properties
configurations {
    compile.exclude module: "spring-boot-starter-tomcat"
}
 
dependencies {
    compile("org.springframework.boot:spring-boot-starter-web:3.0.6")
    compile("org.springframework.boot:spring-boot-starter-jetty:3.0.6")
}
```

>Konfiguration von Datenbanken:
>>Spring Boot bietet eine automatische Konfiguration für Datenbanken.
Es können jedoch auch eigene benutzerdefinierte Konfigurationen erstellt werden,
um spezifische Anforderungen zu erfüllen.
Die DataSource-Konfiguration wird durch Einträge für die Konfigurationseigenschaften

>( spring.datasource.* ) in der Datei application.properties bereitgestellt. Durch die Eigenschaftskonfiguration
wird die Konfiguration vom Anwendungscode entkoppelt. Auf diese Weise können wir die DataSource-Konfigurationen sogar
von externen Konfigurationsanbietersystemen importieren.

application.properties
```properties
# H2 DB
spring.datasource.url=jdbc:h2:file:/tmp/mydb
spring.datasource.username=sa
spring.datasource.password=
spring.datasource.driverClassName=org.h2.Driver
spring.jpa.database-platform=org.hibernate.dialect.H2Dialect

# MySQL
spring.datasource.url=jdbc:mysql://localhost:3306/test
spring.datasource.username=dbuser
spring.datasource.password=dbpass
spring.datasource.driver-class-name=com.mysql.jdbc.Driver
spring.jpa.database-platform=org.hibernate.dialect.MySQL5InnoDBDialect

# Oracle
spring.datasource.url=jdbc:oracle:thin:@localhost:1521:orcl
spring.datasource.username=dbuser
spring.datasource.password=dbpass
spring.datasource.driver-class-name=oracle.jdbc.OracleDriver
spring.jpa.database-platform=org.hibernate.dialect.Oracle10gDialect

# SQL Server
spring.datasource.url=jdbc:sqlserver://localhost;databaseName=springbootdb
spring.datasource.username=dbuser
spring.datasource.password=dbpass
spring.datasource.driverClassName=com.microsoft.sqlserver.jdbc.SQLServerDriver
spring.jpa.hibernate.dialect=org.hibernate.dialect.SQLServer2012Dialect
```
>Oft ist es nicht nötig, den Namen der Treiberklasse anzugeben, da Spring Boot ihn für die meisten Datenbanken aus der Verbindungsurl ableiten kann.

Dies sind einige der Schritte, um Spring Boot manuell zu konfigurieren.
Die automatische Konfiguration von Spring Boot ist jedoch sehr leistungsfähig und sollte in den meisten Fällen ausreichen.
Wenn jedoch eine spezifische Anforderung besteht, kann die Konfiguration von Spring Boot manuell angepasst werden.

#### C. Erstellen eines einfachen Spring Boot-Projekts

Hier sind die Schritte, um ein einfaches Spring-Boot-Projekt zu erstellen:

> 1. Wie unter II A beschrieben kann ein Initiales Projekt mit der starter Seite erstellt werden.

> 2. Importieren des Projektes (öffnen) mittels der IDE z.B. IntelliJ oder VS-Code

> 3. In der Haupt-Klasse des Projektes findet man nun die Annotation **@SpringBootApplication** und
     SpringApplication.run() als Startpunkt der Applikation.

```java
package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }
}
```

> 4. Erstellen eines einfachen Controller für einen API-Endpoint, der eine einfache Begrüßungsnachricht zurückgibt:

``` java
package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {

    @GetMapping("/greeting")
    public String greeting() {
        return "Hello, world!";
    }
}

```

> 5. Die Anwendung kann gestartet werden, indem die DemoApplication-Klasse ausgeführt wird.
     Entweder über die IDE oder bei Nutzung von Maven über das CLI

``` shell
mvn spring-boot:run
```

>6. Die Applikation kann nun mittels Browsers unter der URL http://localhost:8080/greeting
    erreicht werden.
>>Folgende Nachricht sollten nun zu sehen sein "Hello, world!".

Das ist alles, was benötigt wird, um ein einfaches Spring-Boot-Projekt zu erstellen. Weitere Funktionen
können hinzugefügt werden, wie z.B. die Verwendung einer Datenbank oder die Integration von Sicherheit, um die
Anwendung zu erweitern.

Weitere Informationen sind in der offiziellen Spring-Boot-Dokumentation zu finden.
https://docs.spring.io/spring-boot/docs/current/reference/html/

### III. Spring Boot Kernfunktionen

#### A. Inversion of Control (IoC) und Dependency Injection (DI)

>Inversion of Control (IoC) und Dependency Injection (DI) sind zwei Kernkonzepte von Spring Framework und Spring Boot.

IoC ist ein Designmuster, bei dem die Steuerung der Erstellung und Verwaltung von Objekten von einer externen Quelle übernommen wird.

Im Fall von Spring Boot ist diese externe Quelle das Spring-Framework.
IoC ermöglicht eine höhere Flexibilität und eine einfachere Wartung von Anwendungen,
indem es die Abhängigkeiten zwischen Komponenten reduziert und die Wiederverwendbarkeit von Code erhöht.

DI ist ein Konzept, das eng mit IoC verbunden ist. DI bezieht sich auf das Einbringen von Abhängigkeiten
in eine Komponente von außen, anstatt dass die Komponente selbst ihre Abhängigkeiten erstellt oder verwaltet.

Spring Boot unterstützt DI durch die Verwendung von Annotationen wie @Autowired oder @Resource.
Diese Annotationen ermöglichen es, Abhängigkeiten zwischen Komponenten zu definieren und automatisch zu injizieren,
wenn die Komponente erstellt wird.

Zusammen ermöglichen IoC und DI eine lose Kopplung von Komponenten und eine bessere Testbarkeit von Anwendungen.
Spring Boot bietet eine starke Unterstützung für diese Konzepte und erleichtert somit die Entwicklung von Anwendungen,
indem es eine einfache und effektive Möglichkeit zur Verwaltung von Abhängigkeiten bereitstellt.


#### B. Spring Data und Datenbankintegration

> Spring Data ist ein Teil von Spring Framework und bietet eine Abstraktionsschicht für die Datenbankintegration.

Spring Data erleichtert die Entwicklung von Anwendungen, indem es eine standardisierte Möglichkeit zur Interaktion mit Datenbanken bietet.

Spring Data bietet eine Vielzahl von Abstraktionsschichten für verschiedene Datenbanktypen,
darunter **relationale Datenbanken**, **NoSQL-Datenbanken** und **Graphdatenbanken**.

Es unterstützt auch die Integration mit verschiedenen Datenzugriffstechnologien wie
**JDBC**, **JPA**, **MongoDB**, **Redis**, **Elasticsearch** und **Cassandra**.

Durch die Verwendung von Spring Data können Entwickler einfacher und effektiver Datenbankabfragen erstellen und verwalten,
ohne sich auf spezifische Implementierungen von Datenzugriffstechnologien verlassen zu müssen.

Spring Data bietet auch Funktionen wie die Automatisierung von CRUD-Operationen **(Create, Read, Update, Delete)**
und die Unterstützung von Paginierung und Sortierung.

Die Integration von Spring Data in eine Spring-Boot-Anwendung ist einfach.
Zunächst muss die entsprechende Abhängigkeit im Maven-Projekt hinzugefügt werden, wie z.B. spring-boot-starter-data-jpa
für die Integration mit JPA.

Danach können die Entitätsklassen, Repositories und Abfrage-Methoden definiert werden, die die Datenbankabfragen ausführen werden.
Insgesamt bietet Spring Data eine leistungsfähige und flexible Möglichkeit zur Integration von Datenbanken in Spring-Boot-Anwendungen.

> Beispiel für ein Entity Modell Person mit der automatisierten Verbindung via JPA und alle Crud Operationen:

Konfiguration der Pakete:

``` xml
  <dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-jpa</artifactId>
  </dependency>

  <dependency>
    <groupId>org.projectlombok</groupId>
    <artifactId>lombok</artifactId>
    <optional>true</optional>
  </dependency>
  
  <dependency>
    <groupId>com.h2database</groupId>
    <artifactId>h2</artifactId>
    <scope>runtime</scope>
  </dependency>
```

application.properties
``` properties
server.port=${PORT:8080}
spring.datasource.url=jdbc:h2:mem:testdb
spring.jpa.defer-datasource-initialization=true
spring.datasource.driver-class-name=org.h2.Driver
```

Definition der Person Entity in Person.java
``` java 
package com.example.persondemo.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import static jakarta.persistence.GenerationType.SEQUENCE;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Person {
    @Id
    @GeneratedValue(strategy=SEQUENCE, generator="SEQ")
    @SequenceGenerator(name = "SEQ", sequenceName = "PERSON_KEY_SEQ",initialValue = 100,allocationSize = 500)
    private int id;
    private String firstName;
    private String lastName;
    private int age;
}
```

Die entscheidene Automatische Konfiguration und Verbindung zur Datenbank mit allen Standart-CRUD Operationen
wird durch folgende Interface-Definition beschrieben. Das PersonRepository bietet nun alle Zugriffe.

``` java
package com.example.persondemo.repository;

import com.example.persondemo.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends JpaRepository<Person, Integer> {
}
```

Mittels eines Services kann jetzt über das Repository auf die DB Zugegriffen werden.

``` java
package com.example.persondemo.service;

import com.example.persondemo.entity.Person;
import com.example.persondemo.repository.PersonRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonService {

    private final PersonRepository personRepository;

    // Dependency Injection mittels Constructor, es wird das Bean PersonRepository
    // mittels "Autowiring" hier injected
    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public List<Person> getAllPersons() {
      return personRepository.findAll();
    }
}
```

Final noch ein Beispiel für einen Rest-Endpunkt, der den Service nutzt, um Personen mittels
CRUD zu verarbeiten.

``` java
package com.example.persondemo.controller;

import com.example.persondemo.entity.Person;
import com.example.persondemo.service.PersonService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/person")
public class PersonController {

    private final PersonService personService;

    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping("/{id}")
    public Person getPersonById(@PathVariable int id){
        return personService.getPersonById(id);

    }

    @GetMapping("/")
    public Iterable<Person> getAllPersons(){
        return personService.getAllPersons();
    }
}
```

Existiert im resource Ordner eine data.sql Datei, so wird diese in diesem Beispiel automatisch beim
Start der Applikation in die H2-DB importiert.

data.sql im resource Verzeichnis
``` sql
insert into PERSON (ID, FIRST_NAME, LAST_NAME, AGE) values (1,'John','Doe',18);
insert into PERSON (ID, FIRST_NAME, LAST_NAME, AGE) values (2,'Jane','Doe',17);
insert into PERSON (ID, FIRST_NAME, LAST_NAME, AGE) values (3,'Alfred','Neumann',42);
```

Im Weiteren kann auch eine schema.sql Datei im gleichen Verzeichnis angelegt werden, so dass
bei Applikations-Start das Schema angelegt/modifiziert wird.


> Info zu den Annotaions **@Repository**, **@Service**, diese sind Stereotypen von **@Component** und
ermöglichen mittels Autoscan, dass Spring-Boot diese Komponenten findet und Verwaltet. somit können diese
mittels **Injection** direkt verwendet werden **(Autowiring)**


#### C. Web-Entwicklung mit Spring Boot

Spring Boot bietet eine hervorragende Unterstützung für die Web-Entwicklung. Es bietet eine Reihe von Bibliotheken und Funktionen, die die Erstellung von RESTful Web-Services und Web-Anwendungen erleichtern.
>Die wichtigsten Komponenten von Spring Boot für die Web-Entwicklung sind:

>Spring Web MVC:
>> Eine Implementierung des Model-View-Controller (MVC)-Patterns, das es Entwicklern ermöglicht,
Web-Anwendungen zu erstellen, indem es die Verarbeitung von HTTP-Anforderungen und -Antworten aufteilt.

>Spring WebFlux:
>>Eine Implementierung des reaktiven Programmiermodells, die die Verarbeitung von HTTP-Anforderungen
und -Antworten auf nicht-blockierende Weise unterstützt. Dies ermöglicht es, Anwendungen mit höherer Leistung und Skalierbarkeit zu erstellen.

>Spring Security:
>>Eine Bibliothek, die es Entwicklern ermöglicht, Sicherheitsfunktionen wie Authentifizierung und
Autorisierung in ihre Anwendungen zu integrieren.

>Spring REST:
>>Eine Bibliothek, die es Entwicklern ermöglicht, RESTful Web-Services zu erstellen und zu verwalten.

>Spring WebSocket:
>>Eine Bibliothek, die es Entwicklern ermöglicht, WebSocket-basierte Anwendungen zu erstellen.
Um eine Web-Anwendung mit Spring Boot zu erstellen, müssen Entwickler zuerst die entsprechenden Abhängigkeiten im
> Maven-Projekt hinzufügen. Danach kann man die Controller-Klassen definieren, um die Verarbeitung von
> HTTP-Anforderungen
> zu steuern, und die entsprechenden View-Dateien definieren, um die Daten an den Benutzer zurückzugeben.
Zusammenfassend bietet Spring Boot eine leistungsstarke und flexible Plattform zur Entwicklung von Web-Anwendungen und Web-Services.


#### D. Testing in Spring Boot

Testing ist ein wichtiger Bestandteil der Softwareentwicklung, um sicherzustellen, dass Anwendungen korrekt funktionieren und keine unerwarteten Fehler auftreten. Spring Boot bietet eine hervorragende Unterstützung für das Testen von Anwendungen durch eine Vielzahl von Bibliotheken und Frameworks.

>Zu den wichtigsten Komponenten von Spring Boot für das Testen gehören:

>JUnit:
>>Eine Java-Test-Framework-Bibliothek, die zur Erstellung von Unit-Tests verwendet wird.

>Mockito:
>>Eine Java-Test-Framework-Bibliothek, die zur Erstellung von Mock-Objekten für Integrationstests verwendet
wird.

>Spring Test:
>>Eine Bibliothek, die von Spring Boot bereitgestellt wird, um Tests zu vereinfachen und zu unterstützen.

>Testcontainers:
>>Eine Bibliothek, die verwendet wird, um Integrationstests mit isolierten Containern durchzuführen.

>Selenium:
>>Eine Bibliothek, die verwendet wird, um automatisierte End-to-End-Tests von Webanwendungen durchzuführen.

Spring Boot bietet eine Reihe von Anmerkungen und Testwerkzeugen, die es Entwicklern ermöglichen,
Tests einfach und effektiv durchzuführen.

Dazu gehören die Annotationen @SpringBootTest, @WebMvcTest, @DataJpaTest und @RestClientTest,
die Entwicklern helfen, Tests für verschiedene Schichten der Anwendung durchzuführen.

Um Tests in Spring Boot zu erstellen, können Entwickler die verschiedenen Test-Frameworks und Bibliotheken verwenden
und Testklassen definieren, die die Logik für das Testen der Anwendung enthalten. Diese Klassen können dann von den
Build-Tools wie Maven oder Gradle ausgeführt werden, um sicherzustellen, dass die Anwendung korrekt funktioniert.

Insgesamt bietet Spring Boot eine leistungsstarke und flexible Plattform zur Erstellung von Tests für Anwendungen
und erleichtert somit die Erstellung qualitativ hochwertiger und zuverlässiger Software.


### IV. Migration von Java EE zu Spring Boot

#### A. Unterschiede zwischen Java EE und Spring Boot

Java EE (Java Enterprise Edition) und Spring Boot sind beide Frameworks zur Entwicklung von Enterprise-Anwendungen.

>Allerdings gibt es einige Unterschiede zwischen den beiden Frameworks.

>Architektur:
>>Java EE ist ein umfassendes Framework, das eine Vielzahl von APIs und Spezifikationen enthält, die für die
Entwicklung von Enterprise-Anwendungen erforderlich sind. Spring Boot hingegen ist ein schlankeres Framework,
das sich auf die Konfiguration und Verwaltung von Spring-Features konzentriert.

>Modularität:
>>Java EE ist modular aufgebaut und besteht aus einer Vielzahl von APIs und Spezifikationen, die separat
implementiert werden können. Spring Boot hingegen ist ein Framework, das eine umfassende Sammlung
von Bibliotheken und Tools bereitstellt und die Modularisierung über das Teilprojekt Modulith bietet.

>Konfiguration:
>>Java EE verwendet XML-Dateien zur Konfiguration von Anwendungen, während Spring Boot auf die Verwendung von
Annotations und YAML-Dateien setzt, was die Konfiguration und Verwaltung von Anwendungen erheblich vereinfacht.

>Abhängigkeiten:
>>Java EE hat eine umfangreiche Abhängigkeit von den Application Servern und ihrer Implementierung, während Spring Boot
unabhängiger von der zugrundeliegenden Server-Plattform ist und somit auch eine bessere Portabilität ermöglicht.

>Community:
>>Spring Boot hat eine erheblich größere Entwickler-Community als Java EE, was bedeutet, dass es häufiger aktualisiert
wird und mehr Ressourcen und Bibliotheken zur Verfügung stehen.

>Testbarkeit:
>>Spring Boot erleichtert die Testbarkeit von Anwendungen durch die Verwendung von Annotations und Test-Frameworks,
während Java EE Tests schwieriger zu erstellen und durchzuführen machen kann.

Insgesamt bieten beide Frameworks Vorteile und haben ihre eigene Zielgruppe und Anwendungsbereiche.

Spring Boot ist ein Framework, der es Entwicklern ermöglicht sehr effizient Applikationen zu entwickeln, die
unabhängig von ihrer Umgebung ausgeführt werden können. Dadurch dass eine Spring-Boot Applikation "Self-Contained" ist
und inhren egnen Server (Tomcat, Jetty, Netty, etc) mitbringt ist lediglich das JDK notwendig um die Applikation
auszuführen, egal ob auf einem Server, einer virtuellen Maschine oder im Container.

#### B. Migrationsstrategien

Migrationsstrategien beziehen sich auf die verschiedenen Ansätze, die man ergreifen kann,
um von einer vorhandenen Technologie oder Infrastruktur auf eine neue Technologie oder Infrastruktur umzusteigen.

>Bei der Migration von Java EE zu Spring Boot können verschiedene Strategien verfolgt werden, um einen nahtlosen
> Übergang zu gewährleisten.

>Big Bang Migration:
>>Bei dieser Strategie erfolgt die Migration auf einen Schlag, indem alle Komponenten der vorhandenen Anwendung
gleichzeitig auf Spring Boot umgestellt werden. Diese Methode ist schnell, aber auch risikoreich, da jede Komponente der Anwendung
gleichzeitig geändert wird und möglicherweise Probleme auftreten können.

>Phasenweise Migration:
>>Bei dieser Methode wird die Migration in Phasen durchgeführt, wobei jede Phase eine Gruppe von Komponenten der
Anwendung umfasst. Diese Methode ist weniger risikoreich als die Big Bang Migration, da sie schrittweise erfolgt und Zeit für Tests und Fehlerbehebung bietet.

>Parallelbetrieb:
>>Bei dieser Methode wird die neue Anwendung parallel zur alten Anwendung betrieben. Das bedeutet, dass die neue
Anwendung entwickelt und getestet wird, während die alte Anwendung weiterhin in Betrieb ist. Sobald die neue Anwendung bereit ist,
kann sie dann die alte Anwendung ersetzen.

>Hybridansatz:
>>Bei dieser Methode werden beide Technologien parallel betrieben und miteinander integriert. Dies bedeutet, dass
einige Teile der Anwendung auf Java EE und andere Teile auf Spring Boot laufen. Dies ist eine kostengünstige Methode,
da die Migration schrittweise erfolgen kann.

Unabhängig von der gewählten Migrationsstrategie sollten man sicherstellen,
dass ausreichend Zeit und Ressourcen für Tests und Fehlerbehebungen eingeplant werden, um eine reibungslose
Migration zu gewährleisten. Es ist auch wichtig, die Auswirkungen der Migration auf die bestehenden
Prozesse zu berücksichtigen und sicherzustellen, dass Benutzer nicht beeinträchtigt werden.

#### C. Best Practices und Herausforderungen bei der Migration

Bei der Migration von Java EE zu Spring Boot gibt es einige bewährte Verfahren, die man beachten sollten, um
sicherzustellen, dass die Migration reibungslos verläuft und das bestmögliche Ergebnis erzielt wird.

>Hier sind einige Best Practices:

>Analyse und Planung:
>>Bevor die Migration beginnt, sollten man eine gründliche Analyse (SNAP) der bestehenden Java EE-Anwendung durchführen
und eine detaillierte Planung für die Migration erstellen. Dies umfasst die Identifizierung von Abhängigkeiten und die
Bewertung der Komplexität jeder Komponente.

>Automatisierung:
>>Man sollten die Automatisierung nutzen, um den Migrationsprozess zu beschleunigen und Fehler zu minimieren.
Automatisierung kann durch Tools wie Migration-Assistenten, Skripte und Unit-Tests erreicht werden.

>Schrittweise Migration:
>>Man sollten die Migration schrittweise durchführen, indem man zunächst kleinere und weniger komplexe Komponenten
migrieren. Dies ermöglicht eine bessere Kontrolle über den Migrationsprozess und minimiert das Risiko von Fehlern.

>Schulung und Schulungen:
>>Entwickler sollten über die Techniken und Schritte geschult werden, um sicherstellen, dass
sie über die erforderlichen Fähigkeiten und Kenntnisse verfügen, um die neue Technologie zu nutzen und zu unterstützen.

Bei der Migration von Java EE zu Spring Boot können auch einige Herausforderungen auftreten.

>Hier sind einige häufige Herausforderungen:

>Kompatibilitätsprobleme:
>>Es können Kompatibilitätsprobleme zwischen der alten und der neuen Technologie auftreten, insbesondere wenn die
Anwendung älter ist und viele Abhängigkeiten hat.

>Kultureller Wandel:
>>Die Migration auf eine neue Technologie erfordert möglicherweise einen kulturellen Wandel.
Es kann schwierig sein, Entwickler zu überzeugen und zu schulen, die neue Technologie zu nutzen.

>Zeit und Kosten:
>>Die Migration kann zeitaufwändig und teuer sein, insbesondere wenn sie nicht gut geplant und durchgeführt wird.

>Risiko:
>>Eine Migration birgt immer das Risiko von Fehlern und Problemen, die sich auf die Produktivität und die
Benutzbarkeit auswirken können. Man muss sicherstellen, dass ausreichend Zeit und Ressourcen
für Tests und Fehlerbehebung eingeplant werden.


### V. Fortgeschrittene Themen in Spring Boot

#### A. Sicherheit in Spring Boot

Sicherheit ist ein wichtiger Aspekt bei der Entwicklung von Anwendungen, insbesondere wenn es um die Verarbeitung
sensibler Daten geht. Spring Boot bietet verschiedene Funktionen und Konfigurationen, um die Sicherheit von Anwendungen zu erhöhen.

>Hier sind einige Aspekte der Sicherheit in Spring Boot:

>Authentifizierung:
>>Spring Boot bietet verschiedene Optionen zur Authentifizierung von Benutzern, einschließlich
Benutzername/Passwort-Authentifizierung, OAuth2, SAML und LDAP-Authentifizierung.

>Autorisierung:
>>Spring Boot bietet auch Funktionen zur Autorisierung von Benutzern, die den Zugriff auf bestimmte Ressourcen
und Funktionen steuern. Dies kann durch Rollen und Berechtigungen oder durch das Erstellen von benutzerdefinierten
Autorisierungslogik erreicht werden.

>Sicherheitsfilter:
>>Spring Boot bietet eine Vielzahl von Sicherheitsfiltern, die Angriffe wie Cross-Site Scripting (XSS) und
Cross-Site Request Forgery (CSRF) verhindern können.

>Verschlüsselung:
>>Spring Boot unterstützt die Verschlüsselung von Daten, einschließlich Datenbankkennwörtern, Benutzeranmeldedaten
und anderen sensiblen Informationen.

>Fehler- und Ausnahmebehandlung:
>>Spring Boot bietet eine sichere Fehler- und Ausnahmebehandlung, um Angriffe wie Denial-of-Service-Angriffe zu
verhindern.

>Logging und Überwachung:
>>Spring Boot bietet umfangreiche Logging- und Überwachungsfunktionen, um mögliche Sicherheitsprobleme und Angriffe
zu erkennen.

>Aktualisierungen und Patches:
>>Spring Boot veröffentlicht regelmäßig Sicherheitsupdates und Patches, um bekannte Sicherheitslücken zu beheben.

Es ist jedoch wichtig zu beachten, dass die Sicherheit in Spring Boot nicht automatisch gewährleistet ist und dass
Entwickler sicherstellen müssen, dass sie die Sicherheitsfunktionen ordnungsgemäß konfigurieren und implementieren.

Darüber hinaus sollten regelmäßig Sicherheitsprüfungen durchgeführt und Sicherheitslücken behoben werden,
um die Sicherheit der Anwendungen zu gewährleisten.


#### B. Microservices mit Spring Boot

Microservices sind ein Architekturstil, bei dem Anwendungen in kleine, unabhängige Dienste aufgeteilt werden,
die jeweils eine spezifische Funktion erfüllen.

Spring Boot bietet eine Vielzahl von Funktionen und Konfigurationen, um die Entwicklung von Microservices zu unterstützen.

>Hier sind einige Aspekte der Entwicklung von Microservices mit Spring Boot:

>Unabhängigkeit:
>>Microservices sollen unabhängig voneinander funktionieren und entwickelt werden. Spring Boot unterstützt die
Entwicklung von unabhängigen Microservices, indem es eine eingebettete Serverarchitektur bietet, die die Erstellung und Bereitstellung
von Microservices erleichtert.

>Skalierbarkeit:
>>Ein weiteres wichtiges Merkmal von Microservices ist die Skalierbarkeit. Spring Boot bietet Funktionen wie Load
Balancing, Service Discovery und Circuit Breaker, die die Skalierbarkeit von Microservices erhöhen können.

>API-Gateway:
>>Ein API-Gateway ist ein weiteres wichtiges Element in einer Microservices-Architektur. Spring Boot bietet Funktionen
zur Erstellung und Verwaltung von API-Gateways, einschließlich Routing, Load Balancing und Sicherheit.

>Datenmanagement:
>>Microservices müssen in der Lage sein, Daten effektiv zu verwalten und zu kommunizieren. Spring Boot bietet
Funktionen wie Spring Data und Spring Integration, um die Integration von Daten in Microservices zu erleichtern.

>Testbarkeit:
>>Die Testbarkeit von Microservices ist ein wichtiger Aspekt bei der Entwicklung. Spring Boot bietet umfangreiche
Funktionen zur Testautomatisierung und -integration, die die Testbarkeit von Microservices erhöhen können.

>Containerisierung:
>>Containerisierung ist ein wichtiger Aspekt der Bereitstellung von Microservices. Spring Boot unterstützt die
Containerisierung von Microservices durch Docker-Integration und Kubernetes-Unterstützung.

Es ist jedoch wichtig zu beachten, dass die Entwicklung von Microservices mit Spring Boot eine sorgfältige Planung und
Implementierung erfordert, um sicherzustellen, dass die einzelnen Dienste effektiv kommunizieren und die Gesamtleistung
der Anwendung nicht beeinträchtigen.


#### C. Deployment und Skalierung von Spring Boot-Anwendungen

Spring Boot-Anwendungen können auf verschiedene Arten bereitgestellt und skaliert werden, abhängig von den
Anforderungen und Ressourcen der Anwendung.

>Hier sind einige Möglichkeiten, wie Spring Boot-Anwendungen bereitgestellt und skaliert werden können:

>Monolithisch auf einem Server:
>>Eine einfache Möglichkeit, eine Spring Boot-Anwendung bereitzustellen, besteht darin, sie auf einem physischen
oder virtuellen Server zu installieren und auszuführen. Diese Methode ist ideal für kleine Anwendungen oder
Prototypen, bei denen keine hohe Skalierbarkeit erforderlich ist.

>Containerisierung:
>>Containerisierung ist eine der gängigsten Methoden zur Bereitstellung von Anwendungen.
Sie ermöglicht die schnelle und einfache Bereitstellung von Anwendungen auf verschiedenen Plattformen.
Spring Boot-Anwendungen können mit Docker-Containern bereitgestellt werden, die dann auf verschiedenen Plattformen
ausgeführt werden können.


### VI. Abschluss

Insgesamt ist Spring Boot ein Framework, das sich schnell als eine der bevorzugten Optionen für die Entwicklung
von Enterprise-Anwendungen etabliert hat. Es vereinfacht die Entwicklung von Anwendungen, indem es eine Vielzahl von
Funktionen und Bibliotheken bereitstellt, die Entwickler nutzen können, um schnell und effizient robuste und skalierbare
Anwendungen zu erstellen.

In diesem Überblick haben wir einige wichtige Aspekte von Spring Boot behandelt, darunter die Initiierung und
Konfiguration, die Verwendung von Inversion of Control (IoC) und Dependency Injection (DI), Datenbankintegration,
Webentwicklung, Sicherheit, Microservices und Deployment.

Obwohl die Migration von Java EE zu Spring Boot Herausforderungen mit sich bringt, gibt es Migrationsstrategien
und bewährte Verfahren, die helfen können, diese Herausforderungen zu bewältigen. Mit der richtigen Planung und
Vorbereitung können Entwickler erfolgreich von Java EE zu Spring Boot migrieren und die Vorteile dieses Frameworks nutzen.

Insgesamt bietet Spring Boot eine effiziente, robuste und skalierbare Möglichkeit, Anwendungen zu entwickeln,
und ist damit eine der bevorzugten Optionen für die Entwicklung von Enterprise-Anwendungen.


#### A. Zusammenfassung

Zusammenfassend lässt sich sagen, dass Spring Boot ein leistungsstarkes Framework ist,
das sich ideal für die Entwicklung von Enterprise-Anwendungen eignet. Es vereinfacht die Entwicklung durch die
Bereitstellung von Funktionen und Bibliotheken, die Entwickler nutzen können, um schnell und effizient robuste
und skalierbare Anwendungen zu erstellen.

#### B. Ausblick auf weiterführende Themen

>Einige weiterführende Themen im Zusammenhang mit Spring Boot könnten sein:

>Spring Cloud:
>>Eine Sammlung von Tools und Frameworks, die auf Spring Boot aufbauen und Entwicklern dabei helfen, skalierbare und
verteilte Systeme zu erstellen.

>Spring Security:
>>Eine Erweiterung von Spring, die sich auf die Sicherheit von Anwendungen konzentriert und Funktionen
wie Authentifizierung, Autorisierung und Zugriffskontrolle bereitstellt.

>Performance-Optimierung:
>>Möglichkeiten zur Optimierung der Leistung von Spring Boot-Anwendungen, einschließlich der Verwendung von
Caching-Tools und -Techniken, der Optimierung von Datenbankzugriffen und der Verwendung von Cloud-Services.

>Continuous Integration und Continuous Deployment:
>>Die Integration von Spring Boot-Anwendungen in einen Continuous Integration- und Continuous Deployment-Prozess,
um eine schnelle und effiziente Bereitstellung von Anwendungen sicherzustellen.

>Containerisierung:
>>Die Containerisierung von Spring Boot-Anwendungen mithilfe von Docker und Kubernetes, um die Skalierbarkeit,
Portabilität und Wartbarkeit von Anwendungen zu verbessern.

>Diese Themen bieten eine breitere Perspektive auf die Möglichkeiten und Anwendungen von Spring Boot und können für
>>Entwickler hilfreich sein, um die Kenntnisse und Fähigkeiten in diesem Bereich zu vertiefen.
